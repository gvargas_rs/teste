unit UCProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UMD, Data.FMTBcd, Data.DB, Data.SqlExpr,
  RSCore, RSButton, Datasnap.DBClient, URSClientDataSet, Datasnap.Provider,
  Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls, DBEditMaiusc, Vcl.ExtCtrls, RSNavigator,
  Vcl.Grids, Vcl.DBGrids, RSDBGrid, RSLibrary;

type
  TCProduto = class(TForm)
    sqlProduto: TSQLDataSet;
    grid: TRsDBGrid;
    RSNavigator1: TRSNavigator;
    DBEditMaiusc1: TDBEditMaiusc;
    dspProduto: TDataSetProvider;
    cdsProduto: RSClientDataSet;
    dsProduto: TDataSource;
    btNovo: TRSButton;
    btEditar: TRSButton;
    Excluir: TRSButton;
    sqlProdutoID: TIntegerField;
    sqlProdutoEMPCODIGO: TIntegerField;
    sqlProdutoNOMEPRODUTO: TStringField;
    sqlProdutoSALDO: TFloatField;
    sqlProdutoSITUACAO: TIntegerField;
    sqlProdutoVALORCUSTO: TFloatField;
    sqlProdutoVALORVENDA: TFloatField;
    cdsProdutoID: TIntegerField;
    cdsProdutoEMPCODIGO: TIntegerField;
    cdsProdutoNOMEPRODUTO: TStringField;
    cdsProdutoSALDO: TFloatField;
    cdsProdutoSITUACAO: TIntegerField;
    cdsProdutoVALORCUSTO: TFloatField;
    cdsProdutoVALORVENDA: TFloatField;
    procedure btNovoClick(Sender: TObject);
    procedure btEditarClick(Sender: TObject);
    procedure ExcluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    constructor create(AOwner: TComponent); override;
  end;

var
  CProduto: TCProduto;

implementation

{$R *.dfm}

uses
  UIProduto;

procedure TCProduto.btEditarClick(Sender: TObject);
begin
  IProduto := TIProduto.Create(Self, cdsProdutoID.AsInteger);
  try
    IProduto.ShowModal;
  finally
    FreeAndNil(IProduto);
    cdsProduto.Refresh;
  end;
end;

procedure TCProduto.btNovoClick(Sender: TObject);
begin
  IProduto := TIProduto.Create(Self);
  try
    IProduto.ShowModal;
  finally
    FreeAndNil(IProduto);
    cdsProduto.Refresh;
  end;
end;

constructor TCProduto.create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  cdsProduto.Close;
  sqlProduto.ParamByName('EMPCODIGO').AsInteger := MD.Empresa;
  cdsProduto.FetchParams;
  cdsProduto.Open;
end;

procedure TCProduto.ExcluirClick(Sender: TObject);
begin
  if not MessageDlg('Deseja realmente excluir?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    Abort;

  cdsProduto.Delete;
  if cdsProduto.ApplyUpdates(0) > 0 then
    Informacao('Falha ao salvar.');
end;

end.
