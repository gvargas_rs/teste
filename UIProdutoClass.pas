unit UIProdutoClass;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UMD, Data.FMTBcd, Data.DB, Data.SqlExpr,
  Datasnap.DBClient, URSClientDataSet, Datasnap.Provider, Vcl.StdCtrls,
  Vcl.DBCtrls, DBEditCurrency, RSCheckBox, RSDBCheckBox, Vcl.Mask, DBEditMaiusc,
  RSCore, RSButton, RSLibrary, URSProdutoClass, EditMaiusc, EditCurrency;

type
  TIProdutoClass = class(TForm)
    btSalvar: TRSButton;
    btSair: TRSButton;
    edID: TEditMaiusc;
    edProduto: TEditMaiusc;
    ckSituacao: TRSCheckBox;
    edSaldo: TEditCurrency;
    edValorVenda: TEditCurrency;
    edValorCusto: TEditCurrency;
    procedure btSairClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
  private
    Operacao    : TModoOperacao;
    ProdutoClass: TProdutoClass;
  public
    constructor Create(AOwner: TComponent; idProduto: Integer = 0); reintroduce;
  end;

var
  IProdutoClass: TIProdutoClass;

implementation

{$R *.dfm}

procedure TIProdutoClass.btSairClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TIProdutoClass.btSalvarClick(Sender: TObject);
begin
  if edProduto.isEmpty then
    information('Preencimento obrigatório.', edProduto);

  ProdutoClass.ID          := StrToInt(edID.Text);
  ProdutoClass.NomeProduto := edProduto.Text;
  ProdutoClass.Situacao    := iif(ckSituacao.Checked, 1, 0);
  ProdutoClass.ValorCusto  := edValorCusto.value;
  ProdutoClass.ValorVenda  := edValorVenda.value;
  ProdutoClass.Saldo       := edSaldo.value;

  if  not ProdutoClass.post then
  begin
    Informacao('Falha ao salvar.');
    Abort;
  end;

  btSairClick(Sender);
end;

constructor TIProdutoClass.Create(AOwner: TComponent; idProduto: Integer = 0);
begin
  inherited Create(AOwner);
  ProdutoClass       := TProdutoClass.Create(Self, idProduto);
  edID.Text          := IntToStr(ProdutoClass.ID);
  edProduto.Text     := ProdutoClass.NomeProduto;
  ckSituacao.Checked := ProdutoClass.Situacao = 1;
  edValorCusto.value := ProdutoClass.ValorCusto;
  edValorVenda.value := ProdutoClass.ValorVenda;
  edSaldo.value      := ProdutoClass.Saldo;
end;

end.
