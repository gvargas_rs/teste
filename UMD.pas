unit UMD;

interface

uses
  System.SysUtils, System.Classes, Data.DBXFirebird, Data.DB, Data.SqlExpr,
  URSSqlConnection;

type
  TModoOperacao = (mIncluir, mEditar);

  TMD = class(TDataModule)
    connection: RSSQLConnection;
  private
    vEmpCodigo : Integer;
  public
    property Empresa : Integer read vEmpCodigo write vEmpCodigo;
  end;

var
  MD: TMD;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TMD }

end.
