unit UCProdutoClass;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UMD, Data.FMTBcd, Data.DB, Data.SqlExpr,
  RSCore, RSButton, Datasnap.DBClient, URSClientDataSet, Datasnap.Provider,
  Vcl.StdCtrls, Vcl.Mask, Vcl.DBCtrls, DBEditMaiusc, Vcl.ExtCtrls, RSNavigator,
  Vcl.Grids, Vcl.DBGrids, RSDBGrid, RSLibrary, URSProdutoClass;

type
  TCProdutoClass = class(TForm)
    sqlProduto: TSQLDataSet;
    grid: TRsDBGrid;
    RSNavigator1: TRSNavigator;
    DBEditMaiusc1: TDBEditMaiusc;
    dspProduto: TDataSetProvider;
    cdsProduto: RSClientDataSet;
    dsProduto: TDataSource;
    btNovo: TRSButton;
    btEditar: TRSButton;
    Excluir: TRSButton;
    sqlProdutoID: TIntegerField;
    sqlProdutoEMPCODIGO: TIntegerField;
    sqlProdutoNOMEPRODUTO: TStringField;
    sqlProdutoSALDO: TFloatField;
    sqlProdutoSITUACAO: TIntegerField;
    sqlProdutoVALORCUSTO: TFloatField;
    sqlProdutoVALORVENDA: TFloatField;
    cdsProdutoID: TIntegerField;
    cdsProdutoEMPCODIGO: TIntegerField;
    cdsProdutoNOMEPRODUTO: TStringField;
    cdsProdutoSALDO: TFloatField;
    cdsProdutoSITUACAO: TIntegerField;
    cdsProdutoVALORCUSTO: TFloatField;
    cdsProdutoVALORVENDA: TFloatField;
    procedure btEditarClick(Sender: TObject);
    procedure ExcluirClick(Sender: TObject);
    procedure btNovoClick(Sender: TObject);
  private
    ProdutoClass: TProdutoClass;
  public
    constructor create(AOwner: TComponent); override;
  end;

var
  CProdutoClass: TCProdutoClass;

implementation

{$R *.dfm}

uses
  UIProdutoClass;

procedure TCProdutoClass.btEditarClick(Sender: TObject);
begin
  IProdutoClass := TIProdutoClass.create(Self, cdsProdutoID.AsInteger);
  try
    IProdutoClass.ShowModal;
  finally
    FreeAndNil(IProdutoClass);
    cdsProduto.Refresh;
  end;
end;

procedure TCProdutoClass.btNovoClick(Sender: TObject);
begin
  IProdutoClass := TIProdutoClass.create(Self);
  try
    IProdutoClass.ShowModal;
  finally
    FreeAndNil(IProdutoClass);
    cdsProduto.Refresh;
  end;
end;

constructor TCProdutoClass.create(AOwner: TComponent);
begin
  inherited create(AOwner);
  cdsProduto.Close;
  sqlProduto.ParamByName('EMPCODIGO').AsInteger := MD.Empresa;
  cdsProduto.FetchParams;
  cdsProduto.Open;
end;

procedure TCProdutoClass.ExcluirClick(Sender: TObject);
begin
  if not MessageDlg('Deseja realmente excluir?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
    Abort;

  TProdutoClass.delete(cdsProdutoID.AsInteger);
  cdsProduto.Refresh;
end;

end.
