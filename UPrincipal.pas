unit UPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UMD, RSCore, RSButton;

type
  TPrincipal = class(TForm)
    btProduto: TRSButton;
    RSButton1: TRSButton;
    procedure FormCreate(Sender: TObject);
    procedure btProdutoClick(Sender: TObject);
    procedure RSButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Principal: TPrincipal;

implementation

uses
  UCProdutoClass, UCProduto;

{$R *.dfm}

procedure TPrincipal.btProdutoClick(Sender: TObject);
begin
  CProduto := TCProduto.Create(Self);
  try
    CProduto.ShowModal;
  finally
    FreeAndNil(CProduto);
  end;
end;

procedure TPrincipal.FormCreate(Sender: TObject);
begin
  MD.Empresa := 1;
end;

procedure TPrincipal.RSButton1Click(Sender: TObject);
begin
  CProdutoClass := TCProdutoClass.Create(Self);
  try
    CProdutoClass.ShowModal;
  finally
    FreeAndNil(CProdutoClass);
  end;
end;

end.
