unit URSProdutoClass;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Data.DB, Data.SqlExpr, UMD;

type
  TProdutoClass = class
  private
    vID         : Integer;
    vEmpCodigo  : Integer;
    vNomeProduto: string;
    vSaldo      : Currency;
    vSituacao   : Integer;
    vValorCusto : Currency;
    vValorVenda : Currency;
  public
    property ID         : Integer read vID write vID;
    property EmpCodigo  : Integer read vEmpCodigo write vEmpCodigo;
    property NomeProduto: string read vNomeProduto write vNomeProduto;
    property Saldo      : Currency read vSaldo write vSaldo;
    property Situacao   : Integer read vSituacao write vSituacao;
    property ValorCusto : Currency read vValorCusto write vValorCusto;
    property ValorVenda : Currency read vValorVenda write vValorVenda;

    constructor create(AOwner: TComponent; idProduto: Integer = 0);
    class function delete(idProduto: Integer): Boolean;
    function post: Boolean;

  end;

implementation

{ TProdutoClass }

constructor TProdutoClass.create(AOwner: TComponent; idProduto: Integer = 0);
var
  sql: TSQLDataSet;
begin
  sql := TSQLDataSet.create(nil);
  try
    sql.SQLConnection := MD.connection;
    sql.CommandText   := 'select P.ID, P.EMPCODIGO, P.NOMEPRODUTO, P.SALDO, ' +
      ' P.SITUACAO, P.VALORCUSTO, P.VALORVENDA ' +
      'from PRODUTOTESTE P ' +
      'where P.EMPCODIGO = :EMPCODIGO and ' +
      '      P.ID = :ID ';
    sql.ParamByName('EMPCODIGO').AsInteger := MD.Empresa;
    sql.ParamByName('ID').AsInteger        := idProduto;
    sql.Open;

    if not sql.IsEmpty then
    begin
      vID          := sql.FieldByName('ID').AsInteger;
      vEmpCodigo   := sql.FieldByName('EMPCODIGO').AsInteger;
      vNomeProduto := sql.FieldByName('NOMEPRODUTO').AsString;
      vSaldo       := sql.FieldByName('SALDO').AsCurrency;
      vSituacao    := sql.FieldByName('SITUACAO').AsInteger;
      vValorCusto  := sql.FieldByName('VALORCUSTO').AsCurrency;
      vValorVenda  := sql.FieldByName('VALORVENDA').AsCurrency;
    end
    else
    begin
      sql.Close;
      sql.CommandText := 'select gen_id(GEN_PRODUTOTESTE, 1) as ID from RDB$DATABASE ';
      sql.Open;

      vID        := sql.FieldByName('ID').AsInteger;
      vEmpCodigo := MD.Empresa;
    end;

  finally
    FreeAndNil(sql);
  end;
end;

class function TProdutoClass.delete(idProduto: Integer): Boolean;
var
  sql: TSQLDataSet;
begin
  Result := False;

  sql := TSQLDataSet.create(nil);
  try
    sql.SQLConnection := MD.connection;
    sql.CommandText   := 'delete from PRODUTOTESTE ' +
      'where (ID = :ID) and ' +
      '      (EMPCODIGO = :EMPCODIGO) ';
    sql.ParamByName('EMPCODIGO').AsInteger := MD.Empresa;
    sql.ParamByName('ID').AsInteger        := idProduto;
    sql.ExecSQL;
    Result := True;
  finally
    FreeAndNil(sql);
  end;
end;

function TProdutoClass.post: Boolean;
var
  sql: TSQLDataSet;
begin
  Result := False;

  sql := TSQLDataSet.create(nil);
  try
    sql.SQLConnection := MD.connection;
    sql.CommandText   := 'update or insert into PRODUTOTESTE (ID, EMPCODIGO, ' +
      'NOMEPRODUTO, SALDO, SITUACAO, VALORCUSTO, VALORVENDA) ' +
      'values (:ID, :EMPCODIGO, :NOMEPRODUTO, :SALDO, :SITUACAO, :VALORCUSTO, ' +
      ':VALORVENDA) matching (ID, EMPCODIGO) ';
    sql.ParamByName('ID').AsInteger          := vID;
    sql.ParamByName('EMPCODIGO').AsInteger   := vEmpCodigo;
    sql.ParamByName('NOMEPRODUTO').AsString  := vNomeProduto;
    sql.ParamByName('SALDO').AsCurrency      := vSaldo;
    sql.ParamByName('SITUACAO').AsCurrency   := vSituacao;
    sql.ParamByName('VALORCUSTO').AsCurrency := vValorCusto;
    sql.ParamByName('VALORVENDA').AsCurrency := vValorVenda;
    sql.ExecSQL;
    Result := True;
  finally
    FreeAndNil(sql);
  end;
end;

end.
