object CProdutoClass: TCProdutoClass
  Left = 0
  Top = 0
  Caption = 'CProdutoClass'
  ClientHeight = 566
  ClientWidth = 958
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object grid: TRsDBGrid
    Left = 8
    Top = 72
    Width = 942
    Height = 456
    Cursor = crHandPoint
    DataSource = dsProduto
    FixedColor = 15853535
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    CharNotAllowed = '|[]'
    XLSFileDir = 'c:\'
    DefaultRowHeight = 17
    RSOptions = [ColunaReindexar]
    AllowAppend = False
    Zebra = True
    ShowScrowBar = True
    ZebraFontColor = clWindowText
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        width = 55
        Visible = True
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      item
        Expanded = False
        FieldName = 'EMPCODIGO'
        width = 81
        Visible = True
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      item
        Expanded = False
        FieldName = 'NOMEPRODUTO'
        width = 374
        Visible = True
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      item
        Expanded = False
        FieldName = 'SALDO'
        width = 82
        Visible = True
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      item
        Expanded = False
        FieldName = 'SITUACAO'
        width = 124
        Visible = True
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      item
        Expanded = False
        FieldName = 'VALORCUSTO'
        width = 93
        Visible = True
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      item
        Expanded = False
        FieldName = 'VALORVENDA'
        width = 97
        Visible = True
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end>
  end
  object RSNavigator1: TRSNavigator
    Left = 838
    Top = 534
    Width = 112
    Height = 24
    DataSource = dsProduto
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
    ParentShowHint = False
    ConfirmText = 'Deseja excluir o Registro?'
    ShowHint = True
    TabOrder = 1
  end
  object DBEditMaiusc1: TDBEditMaiusc
    Left = 8
    Top = 537
    Width = 817
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    CharNotAllowed = '|[]'
    Caption = ''
    ColorOnFocus = clInfoBk
    ColorOnNotFocus = clWindow
    ColorDisabled = 15853535
    ColorDisabledText = clBlack
  end
  object btNovo: TRSButton
    Left = 8
    Top = 24
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    OnClick = btNovoClick
    Caption = '&Novo'
    TabOrder = 3
  end
  object btEditar: TRSButton
    Left = 87
    Top = 24
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    OnClick = btEditarClick
    Caption = '&Editar'
    TabOrder = 4
  end
  object Excluir: TRSButton
    Left = 166
    Top = 24
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    OnClick = ExcluirClick
    Caption = 'E&xcluir'
    TabOrder = 5
  end
  object sqlProduto: TSQLDataSet
    CommandText = 
      'select P.ID, P.EMPCODIGO, P.NOMEPRODUTO, P.SALDO, P.SITUACAO, P.' +
      'VALORCUSTO, P.VALORVENDA'#13#10'from PRODUTOTESTE P'#13#10'where P.EMPCODIGO' +
      ' = :EMPCODIGO'
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Name = 'EMPCODIGO'
        ParamType = ptInput
      end>
    SQLConnection = MD.connection
    Left = 696
    Top = 160
    object sqlProdutoID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlProdutoEMPCODIGO: TIntegerField
      FieldName = 'EMPCODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlProdutoNOMEPRODUTO: TStringField
      FieldName = 'NOMEPRODUTO'
      ProviderFlags = []
      Size = 100
    end
    object sqlProdutoSALDO: TFloatField
      FieldName = 'SALDO'
      ProviderFlags = []
    end
    object sqlProdutoSITUACAO: TIntegerField
      FieldName = 'SITUACAO'
      ProviderFlags = []
    end
    object sqlProdutoVALORCUSTO: TFloatField
      FieldName = 'VALORCUSTO'
      ProviderFlags = []
    end
    object sqlProdutoVALORVENDA: TFloatField
      FieldName = 'VALORVENDA'
      ProviderFlags = []
    end
  end
  object dspProduto: TDataSetProvider
    DataSet = sqlProduto
    Left = 696
    Top = 216
  end
  object cdsProduto: RSClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspProduto'
    Left = 696
    Top = 264
    object cdsProdutoID: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsProdutoEMPCODIGO: TIntegerField
      DisplayLabel = 'C'#243'digo Empresa'
      FieldName = 'EMPCODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsProdutoNOMEPRODUTO: TStringField
      DisplayLabel = 'Produto'
      FieldName = 'NOMEPRODUTO'
      ProviderFlags = []
      Size = 100
    end
    object cdsProdutoSALDO: TFloatField
      DisplayLabel = 'Quantidade'
      FieldName = 'SALDO'
      ProviderFlags = []
    end
    object cdsProdutoSITUACAO: TIntegerField
      DisplayLabel = 'Situa'#231#227'o'
      FieldName = 'SITUACAO'
      ProviderFlags = []
    end
    object cdsProdutoVALORCUSTO: TFloatField
      DisplayLabel = 'Valor Custo'
      FieldName = 'VALORCUSTO'
      ProviderFlags = []
    end
    object cdsProdutoVALORVENDA: TFloatField
      DisplayLabel = 'Valor Venda'
      FieldName = 'VALORVENDA'
      ProviderFlags = []
    end
  end
  object dsProduto: TDataSource
    DataSet = cdsProduto
    Left = 696
    Top = 312
  end
end
