program AppProduto;

uses
  Vcl.Forms,
  MidasLib,
  UPrincipal in 'UPrincipal.pas' {Principal},
  UCProduto in 'UCProduto.pas' {Produto},
  UIProduto in 'UIProduto.pas' {Form3},
  UMD in 'UMD.pas' {MD: TDataModule},
  UCProdutoClass in 'UCProdutoClass.pas' {CProdutoClass},
  UIProdutoClass in 'UIProdutoClass.pas' {IProdutoClass},
  URSProdutoClass in 'URSProdutoClass.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMD, MD);
  Application.CreateForm(TPrincipal, Principal);
  Application.Run;
end.
