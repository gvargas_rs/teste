unit UIProduto;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, UMD, Data.FMTBcd, Data.DB, Data.SqlExpr,
  Datasnap.DBClient, URSClientDataSet, Datasnap.Provider, Vcl.StdCtrls,
  Vcl.DBCtrls, DBEditCurrency, RSCheckBox, RSDBCheckBox, Vcl.Mask, DBEditMaiusc,
  RSCore, RSButton, RSLibrary;

type
  TIProduto = class(TForm)
    sqlProduto: TSQLDataSet;
    dspProduto: TDataSetProvider;
    cdsProduto: RSClientDataSet;
    dsProduto: TDataSource;
    btSalvar: TRSButton;
    btSair: TRSButton;
    sqlProdutoID: TIntegerField;
    sqlProdutoEMPCODIGO: TIntegerField;
    sqlProdutoNOMEPRODUTO: TStringField;
    sqlProdutoSALDO: TFloatField;
    sqlProdutoSITUACAO: TIntegerField;
    sqlProdutoVALORCUSTO: TFloatField;
    sqlProdutoVALORVENDA: TFloatField;
    cdsProdutoID: TIntegerField;
    cdsProdutoEMPCODIGO: TIntegerField;
    cdsProdutoNOMEPRODUTO: TStringField;
    cdsProdutoSALDO: TFloatField;
    cdsProdutoSITUACAO: TIntegerField;
    cdsProdutoVALORCUSTO: TFloatField;
    cdsProdutoVALORVENDA: TFloatField;
    edID: TDBEditMaiusc;
    edProduto: TDBEditMaiusc;
    edSituacao: TRSDBCheckBox;
    edVtCusto: TDBEditCurrency;
    edVrVenda: TDBEditCurrency;
    edSaldo: TDBEditCurrency;
    procedure btSairClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
  private
    Operacao: TModoOperacao;
  public
    constructor Create(AOwner: TComponent; idProduto: Integer = 0); reintroduce;
  end;

var
  IProduto: TIProduto;

implementation

{$R *.dfm}

procedure TIProduto.btSairClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TIProduto.btSalvarClick(Sender: TObject);
var
  idProduto: Integer;
  sql      : TSQLDataSet;
begin
  if edProduto.isEmpty then
    information('Preencimento obrigatório.', edProduto);

  if Operacao = mIncluir then
  begin
    sql := TSQLDataSet.Create(nil);
    try
      sql.SQLConnection := MD.connection;
      sql.CommandText   := 'select gen_id(GEN_PRODUTOTESTE, 1) as ID from RDB$DATABASE ';
      sql.Open;

      idProduto := sql.FieldByName('ID').AsInteger;
    finally
      FreeAndNil(sql);
    end;

    cdsProdutoID.AsInteger        := idProduto;
    cdsProdutoEMPCODIGO.AsInteger := MD.Empresa;
  end;

  cdsProduto.Post;

  if cdsProduto.ApplyUpdates(0) > 0 then
  begin
    Informacao('Falha ao salvar.');
    Abort;
  end;

  btSairClick(Sender);
end;

constructor TIProduto.Create(AOwner: TComponent; idProduto: Integer = 0);
begin
  inherited Create(AOwner);

  cdsProduto.Close;
  sqlProduto.ParamByName('EMPCODIGO').AsInteger := MD.Empresa;
  sqlProduto.ParamByName('ID').AsInteger        := idProduto;
  cdsProduto.FetchParams;
  cdsProduto.Open;

  if idProduto <> 0 then
  begin
    Operacao := mEditar;
    cdsProduto.Edit;
    Caption := 'Editar - ' + Caption;
  end
  else
  begin
    cdsProduto.Append;
    Caption := 'Incluir - ' + Caption;
  end;
end;

end.
