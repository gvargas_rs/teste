object IProduto: TIProduto
  Left = 0
  Top = 0
  Caption = 'Produto'
  ClientHeight = 152
  ClientWidth = 558
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  DesignSize = (
    558
    152)
  PixelsPerInch = 96
  TextHeight = 13
  object btSalvar: TRSButton
    Left = 8
    Top = 118
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    OnClick = btSalvarClick
    Caption = '&Salvar'
    TabOrder = 0
  end
  object btSair: TRSButton
    Left = 87
    Top = 118
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    OnClick = btSairClick
    Caption = 'S&air'
    TabOrder = 1
  end
  object edID: TDBEditMaiusc
    Left = 8
    Top = 32
    Width = 100
    Height = 21
    DataField = 'ID'
    DataSource = dsProduto
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    MaxLength = 9
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
    CharNotAllowed = '|[]'
    Caption = 'C'#243'digo'
    CaptionRequerido = False
    ColorOnFocus = clInfoBk
    ColorOnNotFocus = clWindow
    ColorDisabled = 15853535
    ColorDisabledText = clBlack
  end
  object edProduto: TDBEditMaiusc
    Left = 136
    Top = 32
    Width = 329
    Height = 21
    Anchors = [akLeft, akTop, akRight]
    DataField = 'NOMEPRODUTO'
    DataSource = dsProduto
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    CharNotAllowed = '|[]'
    Caption = 'Produto'
    ColorOnFocus = clInfoBk
    ColorOnNotFocus = clWindow
    ColorDisabled = 15853535
    ColorDisabledText = clBlack
  end
  object edSituacao: TRSDBCheckBox
    Left = 487
    Top = 34
    Width = 97
    Height = 19
    Transparent = True
    Version = 'Realtec Sistemas - Copyright '#169' 2011'
    Checked = True
    Caption = 'Ativo'
    ParentFont = False
    TabOrder = 4
    Anchors = [akTop, akRight]
    DataField = 'SITUACAO'
    DataSource = dsProduto
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object edVtCusto: TDBEditCurrency
    Left = 8
    Top = 80
    Width = 120
    Height = 21
    DataField = 'VALORCUSTO'
    DataSource = dsProduto
    TabOrder = 5
    WordWrap = False
    Caption = 'Valor Custo'
    ColorOnFocus = clInfoBk
    ColorOnNotFocus = clWindow
    ColorDisabled = 15853535
    ColorDisabledText = clBlack
    AutoSize = False
    DisplayFormat = ',0.00;(,0.00)'
    PosColor = clWindowText
  end
  object edVrVenda: TDBEditCurrency
    Left = 150
    Top = 80
    Width = 120
    Height = 21
    DataField = 'VALORVENDA'
    DataSource = dsProduto
    TabOrder = 6
    WordWrap = False
    Caption = 'Valor Venda'
    ColorOnFocus = clInfoBk
    ColorOnNotFocus = clWindow
    ColorDisabled = 15853535
    ColorDisabledText = clBlack
    AutoSize = False
    DisplayFormat = ',0.00;(,0.00)'
    PosColor = clWindowText
  end
  object edSaldo: TDBEditCurrency
    Left = 292
    Top = 80
    Width = 120
    Height = 21
    DataField = 'SALDO'
    DataSource = dsProduto
    TabOrder = 7
    WordWrap = False
    Caption = 'Quantidade Estoque'
    ColorOnFocus = clInfoBk
    ColorOnNotFocus = clWindow
    ColorDisabled = 15853535
    ColorDisabledText = clBlack
    AutoSize = False
    DisplayFormat = ',0.00;(,0.00)'
    PosColor = clWindowText
  end
  object sqlProduto: TSQLDataSet
    CommandText = 
      'select P.ID, P.EMPCODIGO, P.NOMEPRODUTO, P.SALDO, P.SITUACAO, P.' +
      'VALORCUSTO, P.VALORVENDA'#13#10'from PRODUTOTESTE P'#13#10'where P.EMPCODIGO' +
      ' = :EMPCODIGO and'#13#10'      P.ID = :ID'
    MaxBlobSize = -1
    Params = <
      item
        DataType = ftInteger
        Name = 'EMPCODIGO'
        ParamType = ptInput
      end
      item
        DataType = ftInteger
        Name = 'ID'
        ParamType = ptInput
      end>
    SQLConnection = MD.connection
    Left = 248
    Top = 24
    object sqlProdutoID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlProdutoEMPCODIGO: TIntegerField
      FieldName = 'EMPCODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object sqlProdutoNOMEPRODUTO: TStringField
      FieldName = 'NOMEPRODUTO'
      Required = True
      Size = 100
    end
    object sqlProdutoSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object sqlProdutoSITUACAO: TIntegerField
      FieldName = 'SITUACAO'
      Required = True
    end
    object sqlProdutoVALORCUSTO: TFloatField
      FieldName = 'VALORCUSTO'
    end
    object sqlProdutoVALORVENDA: TFloatField
      FieldName = 'VALORVENDA'
    end
  end
  object dspProduto: TDataSetProvider
    DataSet = sqlProduto
    Left = 288
    Top = 24
  end
  object cdsProduto: RSClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspProduto'
    Left = 328
    Top = 24
    object cdsProdutoID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsProdutoEMPCODIGO: TIntegerField
      FieldName = 'EMPCODIGO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object cdsProdutoNOMEPRODUTO: TStringField
      FieldName = 'NOMEPRODUTO'
      Required = True
      Size = 100
    end
    object cdsProdutoSALDO: TFloatField
      FieldName = 'SALDO'
    end
    object cdsProdutoSITUACAO: TIntegerField
      FieldName = 'SITUACAO'
      Required = True
    end
    object cdsProdutoVALORCUSTO: TFloatField
      FieldName = 'VALORCUSTO'
    end
    object cdsProdutoVALORVENDA: TFloatField
      FieldName = 'VALORVENDA'
    end
  end
  object dsProduto: TDataSource
    DataSet = cdsProduto
    Left = 368
    Top = 24
  end
end
