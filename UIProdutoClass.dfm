object IProdutoClass: TIProdutoClass
  Left = 0
  Top = 0
  Caption = 'IProdutoClass'
  ClientHeight = 157
  ClientWidth = 529
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object btSalvar: TRSButton
    Left = 8
    Top = 118
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    OnClick = btSalvarClick
    Caption = '&Salvar'
    TabOrder = 6
  end
  object btSair: TRSButton
    Left = 87
    Top = 118
    Cursor = crHandPoint
    About = 'Design eXperience. '#169' 2002 M. Hoffmann'
    Version = '1.0.2c'
    OnClick = btSairClick
    Caption = 'S&air'
    TabOrder = 7
  end
  object edID: TEditMaiusc
    Left = 8
    Top = 24
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 0
    Text = 'edID'
    CharNotAllowed = '|[]'
    Caption = 'C'#243'digo'
    ColorOnFocus = clInfoBk
    ColorOnNotFocus = clWindow
    ColorDisabled = 15853535
    ColorDisabledText = clBlack
  end
  object edProduto: TEditMaiusc
    Left = 151
    Top = 24
    Width = 274
    Height = 21
    TabOrder = 1
    Text = 'edProduto'
    CharNotAllowed = '|[]'
    Caption = 'Produto'
    ColorOnFocus = clInfoBk
    ColorOnNotFocus = clWindow
    ColorDisabled = 15853535
    ColorDisabledText = clBlack
  end
  object ckSituacao: TRSCheckBox
    Left = 447
    Top = 26
    Width = 97
    Height = 19
    Transparent = True
    Version = 'Realtec Sistemas - Copyright '#169' 2011'
    Caption = 'Ativo'
    ParentFont = False
    TabOrder = 2
  end
  object edSaldo: TEditCurrency
    Left = 293
    Top = 75
    Width = 120
    Height = 21
    Caption = 'Quantidade Estoque'
    ColorOnFocus = clInfoBk
    ColorOnNotFocus = clWindow
    ColorDisabled = 15853535
    ColorDisabledText = clBlack
    AutoSize = False
    DisplayFormat = ',0.00;(,0.00)'
    PosColor = clWindowText
    TabOrder = 5
  end
  object edValorVenda: TEditCurrency
    Left = 151
    Top = 75
    Width = 120
    Height = 21
    Caption = 'Valor Venda'
    ColorOnFocus = clInfoBk
    ColorOnNotFocus = clWindow
    ColorDisabled = 15853535
    ColorDisabledText = clBlack
    AutoSize = False
    DisplayFormat = ',0.00;(,0.00)'
    PosColor = clWindowText
    TabOrder = 4
  end
  object edValorCusto: TEditCurrency
    Left = 8
    Top = 75
    Width = 121
    Height = 21
    Caption = 'Valor Custo'
    ColorOnFocus = clInfoBk
    ColorOnNotFocus = clWindow
    ColorDisabled = 15853535
    ColorDisabledText = clBlack
    AutoSize = False
    DisplayFormat = ',0.00;(,0.00)'
    PosColor = clWindowText
    TabOrder = 3
  end
end
